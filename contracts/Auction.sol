pragma solidity ^0.4.15;


import './AuctionFactory.sol';

contract Auction {

    address[] public shops;
    AuctionFactory factory; //the owner of the contract will be AuctionFactory
    uint public location;

    event LogTest(string test);
    event LogAddress(address a);
    event LogWinner(string message, address winner);


    function Auction(AuctionFactory f, uint _location, address bikeshop) payable public {
        factory = f; // AuctionFactory
        location = _location;
        if( bikeshop != 0 ) {
            enter(bikeshop);
        }
    }
    
    //a bike makes a deposit to this lottery, will be AuctionFactory eventually
    function deposit() public payable  {
        
        LogTest("Auction >> deposit >> Receiving funds from bike...");

        if(this.balance >= factory.getBikePrice()){
            LogTest("Auction >> deposit >> Going to buy a new bike.");
            endAuction();
        }
                
    }

    //a shop enters lottery to win upcoming bike.
    function enter(address bikeshop) public{
        shops.push(bikeshop); 
        LogTest("Auction >> enter >> Bikeshop entered auction");
    }
    function leave(address bikeshop) public returns (bool){
        
        int found = -1;
        uint i;
        for ( i = 0; i < shops.length; i++){
            if( shops[i] == bikeshop){
                found = int(i);
            }
        }
        if( found == -1 ){
            return false;
        }
        for (i = uint(found); i < shops.length - 1; i++){
            shops[i] = shops[i+1];
        }
        delete shops[shops.length-1];
        shops.length--;
        return true;
    }

    function getShops() public constant returns (address[]){
        return shops;
    }
    function getDeposits() public constant returns (uint){
        return this.balance;
    }
    
    //end the lottery
    function endAuction() private {
        if(shops.length > 0){
            // Random winner from array of bikeshops
            uint winningNumber = uint(block.blockhash(block.number-1)) % shops.length;
            
            // Logging so we can check it in the Bikeshop dapp
            LogWinner("Congratulations! You can now deploy a new Fairbike!", shops[winningNumber]);
            
            // Send the money
            shops[winningNumber].transfer( factory.getBikePrice() );
            
            // Deploy a new bike
            factory.getBikeFactory().addBike();
        }
    }
    
}