pragma solidity ^0.4.15;

import './Shared.sol';
import './BikeFactory.sol';
import './AuctionFactory.sol';

contract Bike {

	Shared.Rental[] history;
	BikeFactory bikeFactory;
	AuctionFactory auctionFactory;
	
	// For now all bikes are in area 1 
	// - until we get geolocation to work
	uint currentLocation = 1;
	uint defaultLocation = 1;

	bytes32 public locationStr = "somewhere";
	uint public locationID = defaultLocation;


	function Bike(BikeFactory bf, AuctionFactory af) public {
		bikeFactory = bf;
		auctionFactory = af;
	}

	function startRent( ) public payable returns (uint) {

		require(msg.value >= bikeFactory.getRentalPrice() );

		require( isAvailable() );
		
		history.push(Shared.Rental(msg.sender, this, now, 0));
		
		// Moved to end-rent
		// Pass the money on to an auction.
		// auctionFactory.deposit.value(msg.value)(currentLocation);

	}
	function endRent(bytes32 _locationStr, uint _locationID ) public returns (bool){

		// LocationStr: intended to be understood by FE
		// LocationID: shared with auction.
		Shared.Rental storage r = history[history.length-1];
		require(r.renter == msg.sender);
		require(r.rentEnd == 0);
		r.rentEnd = now;
		
		locationStr = _locationStr;
		locationID = _locationID;

		// Pass all the money in the contract on to an auction.
		auctionFactory.deposit.value(this.balance)(locationID);
		return true;
	}

	function isAvailable() public constant returns (bool) {
		return (history.length == 0 || history[history.length-1].rentEnd != 0);
	}

	function getRenter() public constant returns (address) {
		if ( isAvailable() ) {
				return 0x00;
		}
		Shared.Rental storage r = history[history.length-1];
		return r.renter;
	}

	function getFactory() public constant returns (address) {
		return bikeFactory;
	}

	function getLocationStr() public constant returns (bytes32){
		return locationStr;
	}
	function getHistory() public constant returns (address, address, uint, uint) {
			require(history.length != 0);
			Shared.Rental storage r = history[history.length-1];
			return (r.renter, r.bike, r.rentStart, r.rentEnd);
	}

}
