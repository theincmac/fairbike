pragma solidity ^0.4.15;

// import './Shared.sol';
import './Auction.sol';
import './BikeFactory.sol';

contract AuctionFactory {

    mapping (uint => Auction) auctions;
    uint bikePrice;
    address owner;
    BikeFactory bikeFactory;

    Auction[] public auctionAddresses;

    event LogTest(string test);
    event LogAddress(address a);

    function AuctionFactory( BikeFactory bf ) public{
        LogTest("AuctionFactory >> constructor");
        owner = msg.sender;
        bikeFactory = bf;
        // bikePrice = bikeFactory.call.getBikePrice();
        bikePrice = 1 ether;

    }
    function deposit(uint location) public payable{

        LogTest("Auction >> deposit >> making deposit");

        if(auctions[location] == Auction(0)){
            LogTest("Auction >> deposit >> addAuction");
            addAuction(location, 0);
        }else {
            LogTest("Auction >> deposit >> deposit to existing");
            Auction(auctions[location]).deposit.value(msg.value)();
        }
        
    }
    function enter(uint location) public{
        if(auctions[location] == Auction(0)){
            addAuction(location, msg.sender);
        } else {
            auctions[location].enter(msg.sender);
        }
    }
  function getAuctions() public constant returns (Auction[]) {   
    return auctionAddresses; 
  }
  function getBikePrice() public constant returns (uint) {
    return bikePrice;
  }

  function addAuction(uint location, address bikeshop) private returns (Auction) {
     Auction a = (new Auction).value(msg.value)(this, location, bikeshop);
     auctions[location] = a;
     auctionAddresses.push(a);
     return a;
  }
  function getBikeFactory() public constant returns (BikeFactory) {
      return bikeFactory;
  }

  function terminate() public{
    if(msg.sender == owner){
        selfdestruct(owner);
    }
  }

  function() payable public {
    require(false);
  }

}

/*
    Auction factory
    - accept incoming deposits from Bikes
        - payload has: lat/long in uint32
    - makes instances of Auction
        - with lat/long rounded to 1 decimal
        - it's own adress, in case Auction expires

    Auction
    - accept incoming deposits from Auction-factory (mvp: bike)
    - has property: lat/long
    - has property: list of subscribers
    - when condition is met: wire all funds to random subscriber
    - when there are no shops.. what to do?
        - the over-budger: refund to the Auction-factory

    example precision: 1 decimal
    Rotterdam 
    51.9, 4.4

*/




















