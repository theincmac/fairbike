pragma solidity ^0.4.15;

import './Shared.sol';
import './Bike.sol';
import './AuctionFactory.sol';

contract BikeFactory {
  Bike[] public bikes;
  AuctionFactory public auctionFactory;
  
  // around 1 eur at current exchange rate
  uint public rentalPrice = 0.003 ether; 
  uint public bikePrice = 1 ether; 

  address owner;

   event LogTest(string test);
   event LogAddress(address a);

  function BikeFactory() public {
      LogTest('Bike Factory ready');

      owner = msg.sender;
      
      auctionFactory = new AuctionFactory(this);
      
      addBike();
  }

  function getRentalPrice() public constant returns (uint) {
    return rentalPrice;
  }
  function getBikePrice() public constant returns (uint) {
    return bikePrice;
  }

  function addBike() public returns (uint) {

    //check if the sender is legit??

    Bike b = new Bike(this, auctionFactory);
    LogAddress(b);
    bikes.push(b);
    
  }

  function getBikes() public constant returns (Bike[]) {
    return bikes;
  }
}
