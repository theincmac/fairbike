pragma solidity ^0.4.17;

contract Main {
   Sub public sub;
   event LogTest(string test);
   event LogAddress(address a);

  function Main() payable public {
    //sub = new Sub();
  }

  function deposit() payable public {
    //sub = new Sub();
    //sub.deposit.value(msg.value)();
   
    //sub.deposit.value(msg.value)();
    sub = (new Sub).value(msg.value)();
  }

  function withdrawSend() public {
    sub.withdrawSend(msg.sender);
  }
  function withdrawTransfer() public {
    sub.withdrawTransfer( msg.sender );
  }
  function withdrawSelfdestruct() public {
    sub.withdrawSelfdestruct( msg.sender );
  }
  function getBalance() public constant returns (uint) {

    return sub.getBalance();
  }
  function getOwnBalance() public view returns (uint){
     return this.balance;
  }
}


contract Sub {
  //uint public balance = 10;

  event LogTest(string test);
  event LogAddress(address a);
  
  function Sub() payable public {
    LogTest('Sub constructor');
  }

  function deposit() payable public returns (uint) {
     LogTest('sub.deposit');
    //balance = msg.value;
    return this.balance;
  }
  function withdrawSend( address a ) public {
     LogTest('withdraw via send');
     LogAddress(a);

     bool success = a.send(1 ether);
     if( success ){
         LogTest('success');
     } else {
         LogTest('fail');
     }
  }
  function withdrawTransfer( address a ) public {
    LogTest('withdraw via transfer');
    LogAddress(a);
    a.transfer(1 ether);
  }
  function withdrawSelfdestruct( address a ) public {
     LogTest('withdraw via selfdestruct');
     LogAddress(a);
     selfdestruct(a);
  }
  function getBalance() public constant returns (uint) {
    return this.balance;
  }
}
