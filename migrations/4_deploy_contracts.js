var Bike = artifacts.require("./Bike.sol");
var BikeFactory = artifacts.require("./BikeFactory.sol");

var Auction = artifacts.require("./Auction.sol");
var AuctionFactory = artifacts.require("./AuctionFactory.sol");


var Shared = artifacts.require("./Shared.sol");

module.exports = function(deployer) {
  deployer.deploy(Shared);
  deployer.link(Shared, [Bike, BikeFactory]);

  deployer.deploy(BikeFactory);
  deployer.deploy(AuctionFactory);
};
