// Import the page's CSS. Webpack will know what to do with it.
import "./app.css";
import "./loader.css";
import "./ui.css";


import "./ui.js";

// Import libraries we need.
import {default as Web3} from 'web3';
import {default as contract} from 'truffle-contract'

import bike_artifacts from '../build/contracts/Bike.json'
import bikefactory_artifacts from '../build/contracts/BikeFactory.json'

var Bike = contract(bike_artifacts);
var BikeFactory = contract(bikefactory_artifacts);

var accounts;
var account;
var factory;



window.App = {
	start: function() {
		var self = this;
		this.bikes = [];

		BikeFactory.setProvider(web3.currentProvider);
		Bike.setProvider(web3.currentProvider);

		web3.eth.getAccounts(function(err, accs) {
			if (err != null) {
				alert("There was an error fetching your accounts.");
				return;
			}

			if (accs.length == 0) {
				alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
				return;
			} else {
				console.log("ready to go!");
			}

			accounts = accs;
			account = accounts[0];
			window.accounts = accounts;
			self.initFactory();

		});
	},

	initFactory: function() {
		var self = this;
		BikeFactory.deployed().then(function(instance) {
			console.log(instance);
			self.factory = instance;
			self.getBikes();
		}).catch(function(e) {
			console.log(e);
		});
	},

	getBikes: function() {
		var self = this;

		self.factory.getBikes.call(account)
		.then( ar => {

			//populate bike objects
			self.bikes = ar.map( addr => ({
				address: addr,
				id: addr.substr(2,6),
				instance: Bike.at(addr)
			}));

			// load availability (async)
			self.loadBikeAvailability();
			self.loadBikeLocation();

		}).catch( console.log );
	},
	loadBikeAvailability: function() {
		var self = this;

		// Wait for all bike availability to load
		// before loading the renters
		Promise.all(
			self.bikes.map( bikeObj => {
				return bikeObj.instance.isAvailable.call().then( v => {
					bikeObj.available = v;
				})
			})
		)
		// Next step: load the renters
		.then( () => {
			self.loadBikeRenters()
		})
		.catch( console.log );
	},
	loadBikeLocation : function(){
		var self = this;

		Promise.all(
			self.bikes.map( bikeObj => {
				return bikeObj.instance.getLocationStr.call().then( l => {
					console.log("location: ",l);
					bikeObj.locationStr = l;

				})
			})
		)
	},
	loadBikeRenters: function() {
		var self = this;

		// Wait for all renters to load before displaying the bikes
		Promise.all(
			self.bikes.map( (bikeObj, i) => {
				if (bikeObj.available) return;
				return bikeObj.instance.getRenter.call().then( r => {
					bikeObj.renter = r;
				});
			})
		)
		// Next step: display the bikes
		.then( () => {
			self.displayBikes();
		})
		.catch( console.log );

	},
	displayBikes: function(){
		var self = this;
		self.hideLoader();
		var bikesEl = document.getElementById('bikes');

		var templateFn = function(bike){

			var templ = `<li id="${bike.id}" class="bike">${bike.address} `
			
			templ += ` - ${bike.locationStr}`;

			if ( bike.available ){
				templ +=  '<button class="rent">Rent</button>';
			} else if (bike.renter == account ) {
				templ += '<button class="end-rent">End rent</button>';
			} else {
				templ += '<i>Rented out</i>'
			}
			templ += '</li>';
			return templ;
		}

		var htmlStr = '<ul>';
		htmlStr += self.bikes.map( templateFn ).join('\n');
		htmlStr += '</ul>';

		bikesEl.innerHTML = htmlStr;

		self.addListeners();
	},
	addListeners: function() {
		var self = this;

		var rentListener = function(e) {
			console.log(e);
			//self.getPosition(console.log);
			self.startRent(e.target.parentElement.id);
		}
		var endRentListener = function(e) {
			console.log(e);

			self.endRent(e.target.parentElement.id);
		}

		Array.from( document.querySelectorAll('button.rent'))
		.forEach( btn => {
			btn.addEventListener('click', rentListener);
		});

		Array.from( document.querySelectorAll('button.end-rent'))
		.forEach( btn => {
			btn.addEventListener('click', endRentListener);
		});

	},
	startRent: function( id ) {
		console.log('startRent');
		var self = this;
		this.bikes.find( b => {
			if( b.id === id ){
				self.showLoader();
				b.instance.startRent({
					value: web3.toWei('0.003', 'ether'),
					from: account
				})
				.then( res => {
					console.log('rent started', res);
					b.available = false;
					b.renter = account;
					self.displayBikes();
				})
				.catch( er => {
					console.log(er);
					self.hideLoader();
				});
				return true;
			}
		})
	},
	endRent: function(id) {
		var self = this;
		console.log('endRent');
		this.bikes.find( b => {
			if( b.id === id ){
				self.showLoader();
				b.instance.endRent(
					"somewhere over the rainbow",
					2,
				{
					from: account,
					
				})
				.then( res => {
					console.log('rent ended', res);
					b.available = true;
					b.renter = null;
					self.displayBikes();
				})
				.catch( console.log );
				return true;
			}
		})
	},
	showLoader: function(){
		document.getElementById('loader').style.display = "block";
	},
	hideLoader: function(){
		document.getElementById('loader').style.display = "none";
	},
	getPosition: function(callback){
		var self = this;
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition( function( pos) {
				callback(pos.coords.latitude, pos.coords.longitude);
			}, console.log);
		} else {
			/* geolocation IS NOT available */
			callback('geolocation not available');
		}
	}
}

window.addEventListener('load', function() {
	// Checking if Web3 has been injected by the browser (Mist/MetaMask)
	if (typeof web3 !== 'undefined') {
		console.warn("Using web3 detected from external source. If you find that your accounts don't appear ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
		// Use Mist/MetaMask's provider
		window.web3 = new Web3(web3.currentProvider);
	} else {
		console.warn("No web3 detected. Falling back to http://localhost:8546. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
		// fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
		window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8546"));
	}

	App.start();
});
