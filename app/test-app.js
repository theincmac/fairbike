// Import the page's CSS. Webpack will know what to do with it.
import "./app.css";

// Import libraries we need.
import {default as Web3} from 'web3';
import {default as contract} from 'truffle-contract'

import bike_artifacts from '../build/contracts/Bike.json'
import bikefactory_artifacts from '../build/contracts/BikeFactory.json'

var Bike = contract(bike_artifacts);
var BikeFactory = contract(bikefactory_artifacts);

var accounts;
var account;
var factory;
window.accounts = accounts;

window.App = {
  start: function() {
    var self = this;

    BikeFactory.setProvider(web3.currentProvider);
    Bike.setProvider(web3.currentProvider);

    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      account = accounts[0];
      window.accounts = accounts;
      self.init();

    });
  },
  init: function() {
    var self = this;
    self.initFactory().then(factory => {
      self.factory = factory;
      self.testAndGetBikes().then(addr => {
        self.checkAndRentBike(addr)
      })
    });
  },

  initFactory: function() {
    return new Promise((resolve, reject) => {
      BikeFactory.deployed().then(function(instance) {
        console.log(instance);
        resolve(instance);
      }).catch(function(e) {
        console.log(e);
        reject(e);
      });
    })
  },
  testAndGetBikes: function(factory) {
    var self = this;
    return new Promise((resolve, reject) => {
      self.factory.getRentalPrice.call(account).then(function(value) {
        console.log('Test call rental price:', value.toNumber());
        return self.factory.getBikes.call(account);
      }).then(function(value) {
        console.log('getBikes:', value);
        resolve(value[value.length - 1]);
      }).catch(function(e) {
        console.log(e);
        reject();
      });
    });
  },
  addBike: function() {
    var self = this;
    return new Promise((resolve, reject) => {
      return self.factory.addBike({from: account}).then(v => {
        console.log(v);
        return self.factory.getBikes.call(account);
      }).then(v => {
        console.log('getBikes:', v);
        resolve(v[v.length - 1]);
      }).catch(function(e) {
        console.log(e);
        reject();
      });
    });
  },
  checkAndRentBike: function(address) {
    console.log(address);
    console.log(account);
    console.log(typeof account);
    var self = this;
    self.bike = Bike.at(address);
    self.bike.isAvailable.call().then(v => {
      console.log(v);
      return self.bike.getFactory.call();
    }).then(v => {
      console.log('fact?', v);
      return self.bike.getHistory.call();
    }).then(v => {
      console.log(v);
      return self.bike.startRent('0xcB56Fe207C77dbf37E8c6a2F681f3Df877179e5E', {
        value: web3.toWei('0.5', 'ether'),
        from: account
      });
    }).then(v => {
      console.log('rent result:', v);
    }).catch(e => {
      console.log('error:', e)
    });
  }
};

window.addEventListener('load', function() {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8546. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8546"));
  }

  App.start();
});
