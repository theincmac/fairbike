// Allows us to use ES6 in our migrations and tests.
require('babel-register')

var HDWalletProvider = require("truffle-hdwallet-provider");

var infura_apikey = "sRRkjPHKDv3ne4zFmEv9";
var mnemonic = "call spray latin diary know orange punch second trophy glide raven donate";


module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8546,
      network_id: '*' // Match any network id
    },
    ropsten: {
      // host: "localhost",
      // port: 8545,
      provider: new HDWalletProvider(mnemonic, "https://ropsten.infura.io/"+infura_apikey),
      network_id: "3"
      // gas: 3000000
    }
  },
  solc: '/usr/bin/solc'
}
